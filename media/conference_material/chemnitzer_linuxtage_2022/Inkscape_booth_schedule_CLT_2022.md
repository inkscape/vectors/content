# Talks at the Inkscape booth

## Saturday, March 12


Time: 10:00 – 10:30  
Title: Inking with Inkscape Part One - Prepare Inkscape for Inking work-flow  
Speaker: Tim Jones  
Language: Englisch  
Description: Part One - Prepare Inkscape for a traditional Inking work-flow.

Time: 10:30 – 11:00  
Title: Inking with Inkscape Part Two - Outlining with Mouse, and Pen-Tablet  
Speaker: Tim Jones  
Language: Englisch  
Description: Part Two - Outlining with Mouse, and Pen-Tablet.

Time: 11:00 – 12:00  
Title: Inking with Inkscape Part Three - Cross Hatching  
Speaker: Tim Jones  
Language: Englisch  
Description: Part Three - Traditional Cross Hatching techniques, in Inkscape.

Time: 12:00 – 12:30  
Title: Inking with Inkscape Part Four - Finishing  
Speaker: Tim Jones  
Language: Englisch  
Description: Part Four - Finalising the Drawing. Adding paper texture.

Time: 13:00 – 13:30  
Title: Introduction To Computer Graphics For Complete Newbies  
Speaker: Mihaela Jurković  
Language: Englisch  
Description: This talk explains the basic concepts you need to understand to start your computer graphics journey. Learn about the fundamental differences between graphics types, when to use which type, which software to use in different cases, the concept of objects and layers as building blocks of your drawings. These concepts are universal, applicable to any graphics software. Examples in this talk use Inkscape and GIMP.

Time: 14:00 – 14:30  
Title: How To Use Inkscape For Web Design  
Speaker: Mihaela Jurković  
Language: Englisch  
Description: The world of web design seems to be dominated by proprietary software, but there are FOSS solutions that can easily compete with them. Inkscape is one of those tools. Inkscape is powerful and incredibly versatile, and in this talk we'll show how it can be set up to be your default web design tool. We'll illustrate how to best use Inkscape and its features for common web design processes. Topics we'll cover: brand color palettes, grids and guides for responsive web layout, design components in layers, how to use clones wisely, and how to jump start new projects with templates.

Time: 14:30 – 15:00  
Title: What I've learned through Contributing Skills (Other than Coding) to the Inkscape Project  
Speaker: Michèle Thibeau  
Language: Englisch  
Description: Above and beyond learning about open-source software and communities, being a contributor to the Inkscape Project has enabled me to collaborate with people around the world. Together, through bringing our skills to the table, we’re advancing this complex and beautiful tool that offers everyone who is interested the ability to Draw Freely.

Time: 15:30 – 16:00  
Title: Design Your Own Logo and T-Shirt in 30 Minutes  
Speaker: Ryan Gorley  
Language: Englisch  
Description: See how to take a logo from concept to completion, then how to prepare artwork for printing on a t-shirt. This demonstration should be helpful for those with limited experience using Inkscape, but it will be fun for everyone in a Bob Ross kind of way. "We don't make mistakes, we have an undo button."

Time: 16:00 – 16:30  
Title: Inkscape A-Z, Teil I: A-F  
Speaker: Maren Hachmann  
Language: Deutsch oder Englisch nach Bedarf  
Description: Im ersten von vier Vorträgen über spannende und weniger bekannte Inkscape-Funktionen geht es um die Themen 'Ausrichten am Kreis', 'Befehlseingabe', 'CSS', 'Druckempfindliche Werkzeuge, 'Exportformate' und 'Fonts gestalten'.

Time: 17:00 – 17:30  
Title: Inkscape A-Z, Teil II: G-M  
Speaker: Maren Hachmann  
Language: Deutsch oder Englisch nach Bedarf  
Description: Im zweiten von vier Vorträgen über spannende und weniger bekannte Inkscape-Funktionen geht es um die Themen 'Gitter', 'Hilfsbereite und kreative Community', 'Iconvorschau', 'Justierbare Filter', 'Kalender', 'Lasern, Sticken, Fräsen, Schneiden, Plotten' und 'Muster selbermachen'.


## Sunday, March 13


Time: 11:00 – 11:30  
Title: Wie du zum Inkscape Entwickler wirst  
Speaker: Thomas Holder  
Language: Deutsch oder Englisch nach Bedarf  
Description: Geschichte der Inkscape Entwicklung - Programmiersprachen und Bibliotheken - Buildsystem und Build Tools - GitLab - Kommunikation mit anderen Entwicklern

Time: 11:30 – 12:00  
Title: Getting started as an extensions developer  
Speaker: Thomas Holder  
Language: Deutsch oder Englisch nach Bedarf  
Description: Wie funktionieren Inkscapes "Extension"-Plugins? Erstellen einer Erweiterung mit Python.

Time: 13:00 – 13:30  
Title: Introduction To Computer Graphics For Complete Newbies  
Speaker: Mihaela Jurković  
Language: Englisch  
Description: This talk explains the basic concepts you need to understand to start your computer graphics journey. Learn about the fundamental differences between graphics types, when to use which type, which software to use in different cases, the concept of objects and layers as building blocks of your drawings. These concepts are universal, applicable to any graphics software. Examples in this talk use Inkscape and GIMP.

Time: 14:00 – 14:30  
Title: Inkscape A-Z, Teil III: N-T  
Speaker: Maren Hachmann  
Language: Deutsch oder Englisch nach Bedarf  
Description: Im dritten von vier Vorträgen über spannende und weniger bekannte Inkscape-Funktionen geht es um die Themen 'Nummerneingabe', 'Oberfläche anpassen', 'Perspektive', 'QR- und andere Codes', 'Röntgenmodus und weitere Ansichtsmodi', 'Symbolbibliotheken' und 'Tastaturkürzel ändern'

Time: 14:30 – 15:00  
Title: How To Use Inkscape For Web Design  
Speaker: Mihaela Jurković  
Language: Englisch  
Description: The world of web design seems to be dominated by proprietary software, but there are FOSS solutions that can easily compete with them. Inkscape is one of those tools. Inkscape is powerful and incredibly versatile, and in this talk we'll show how it can be set up to be your default web design tool. We'll illustrate how to best use Inkscape and its features for common web design processes. Topics we'll cover: brand color palettes, grids and guides for responsive web layout, design components in layers, how to use clones wisely, and how to jump start new projects with templates.

Time: 15:00 – 15:30  
Title: Inkscape A-Z, Teil IV: U-Z  
Speaker: Maren Hachmann  
Language: Deutsch oder Englisch nach Bedarf  
Description: Im vierten von vier Vorträgen über spannende und weniger bekannte Inkscape-Funktionen geht es um die Themen 'Unicodeeingabe und Tabellen', 'Vektorisieren', 'Webgrafiken animieren', 'XML-Editor', 'Yeah! Effekte!' und 'Zen-Modus'.

Time: 15:30 – 16:00  
Title: Growing my surface pattern design practice with Inkscape  
Speaker: Michèle Thibeau  
Language: Englisch  
Description: I use Inkscape to draw digital elements to take advantage of designing vectors. It seems like Inkscape and I have grown over the years. During this short talk, I’ll share a bit about my practice, some of my work and what I love most about working with Inkscape.

Time: 16:00 – 16:30  
Title: Inking with Inkscape Part One - Prepare Inkscape for Inking work-flow  
Speaker: Tim Jones  
Language: Englisch  
Description: Part One - Prepare Inkscape for a traditional Inking work-flow.

Time: 16:30 – 17:00  
Title: Inking with Inkscape Part Two - Outlining with Mouse, and Pen-Tablet.
Speaker: Tim Jones  
Language: Englisch  
Description: Part Two - Outlining with Mouse, and Pen-Tablet.

Time: 17:00 – 18:00  
Title: Inking with Inkscape Part Three - Cross Hatching  
Speaker: Tim Jones  
Language: Englisch  
Description: Part Three - Traditional Cross Hatching techniques, in Inkscape.

Time: 18:00 – 18:30  
Title: Inking with Inkscape Part Four - Finishing  
Speaker: Tim Jones  
Language: Englisch  
Description: Part Four - Finalising the Drawing. Adding paper texture.
