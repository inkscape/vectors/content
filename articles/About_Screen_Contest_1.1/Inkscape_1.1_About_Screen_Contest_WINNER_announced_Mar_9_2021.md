Inkscape 1.1 About Screen Contest Winner Announced March 9, 2021



# Fauzan Syukri's "Art Bot" wins hearts & votes for 1.1 About Screen 

Congratulations to Inkscape artist Fauzan Syukri of Pariaman, Indonesia, for "Art Bot", which will be featured in Inkscape 1.1 as the About Screen.

Fauzan is not new to our About Screen Contest. He submitted a second entry to this contest, "??" and two entries for the 0.92 edition, one which you can see [here](https://inkscape.org/es/~fauzan291/%E2%98%85ngarai-sianok). You can see more of Fauzan's work on [Behance]([fauzan Syukri on Behance](https://www.behance.net/Ozant/projects)) and on [Facebook](https://www.facebook.com/ozant.liuky).

Special thanks to our finalists Ray Oliveira, Antonio Santiago and Luiz Lucas for their outstanding creations.

A whopping ?? Inkscape contributors and 411 community voters participated in this traditional release contest. Thanks to each Inkscape artist who entered the 1.1 contest, many of whom used [Inkscape 1.1 alpha](https://inkscape.org/release/inkscape-1.1/?latest=1) to create their art. 

The project is aiming to launch Inkscape version 1.1 this spring. Head here to [learn more about what new and improved items](https://wiki.inkscape.org/wiki/index.php/Release_notes/1.1) will be shipped in our next launch! 

In the meantime, Draw Freely.



*Interested in contributing to the Inkscape project? [Visit us online](https://inkscape.org/contribute/) to find out how you can make a difference by joining our community or donating to support our work!*



Note: attempt to remove hyphenation on web page. 