## Welcome to Inkscape 1.1!

### What's new and what you can now do



Among the highlights in Inkscape 1.1 are a Welcome dialogue, a Command Palette, a revamped Dialog Docking System, and searchable preference options, along with new formats for exporting your work. 

Here at the Inkscape project, we're proud to have contributors from around the world who invest their time, energy and skills towards coding, debugging, translating, documenting and promoting the program.

Built mostly with [**the power of a team of volunteers**](https://inkscape.org/news/2020/05/04/roots-and-shoots-inkscape-project/), this open source vector editor represents the work of many hearts and hands from around the world, ensuring that Inkscape remains available free for everyone to download and enjoy. Thanks to the 2020 cohort of Google Summer of Code (GSoC) students (and their mentors) who worked hard to deliver some of the features and bug fixes in 1.1. 

Let's dive into some highlights and tour what's new and what you can now do with the latest version of Inkscape!



#### A warm welcome

A brand new Welcome screen awaits you upon launching Inkscape 1.1! The Welcome Dialog allows users to customize their experience by choosing canvas colors, keyboard shortcut styles, theme sets and color modes. Choose the size of the document you want to create - for print, social media and more - along with the perfect screen size, or open a recent file. Additionally, find out how to support Inkscape by becoming a contributor to the project. 



#### In command of your search

Our new Command Palette opens with the touch of the ? key and enables you to search and use a variety of functions without having to resort to menus or shortcuts. You can edit, rotate and reset, among other commands that have already been converted to 'actions'. More will be rolled out in future versions of the program. Search for commands in both English and your local language, too. You can also import or open files from Inkscape's document history. This feature was added by GSoC student Abhay Raj Singh in 2020.



#### A next-level docking experience

Our revamped Dialog Docking System works better and is more versatile, allowing you to dock your dialog boxes on either side of the workspace. They are displayed as tabs, with optional labels and icons. When you've got a number of tabs open, they transform into icons. Combine your favorite tabs and position your floating docks.  Close them using the middle mouse button, or use F12 to toggle all dialogs at once. Best of all, Inkscape now remembers your choices across sessions. Work on this big code refactoring was undertaken by GSoC student Valentin Ionita in 2020.



#### Search preferences easily

You're going to appreciate the search bar that allows you to find the preference setting you're looking for almost instantaneously. The new search bar feature was developed by this year's GSoC student Parth Pant. The zoom correction factor now works in different measurements and has been factored into the zoom level to accurately reflect the real world scale of your work. Additional preference settings deliver you more freedom of choice.



#### Double check with new Display Mode

For those of you wrangling drawings with many layers, our new outline overlay mode is worth investigating. It displays a muted view (opacity) of your drawing behind the object outlines that are click-sensitive, so you can make sure everything is in order. This new mode was developed by Jabier Arraiza. 



#### What's new with Drawing tools, Nodes, Connectors & Selector

The Calligraphy Tool has gained in precision, while connection lines drawn with the Connector Tool now update in real time. You can now copy, cut and paste a selection of nodes with the Node Tool - insert them into the original path, a different one or make a completely new one. Experiment with the Pen / Pencil Tool's new "Scale" option to set a path width created with a "Shape" option numerically. Activate the new selection mode for the lasso/rubberband selection and select every object within the box or touching its boundaries. 


#### Introducing the Slice (& dice) Live Path Effect (LPE)

This new path effect allows you to split an object into two or more parts without destroying the original. You can change styles on both parts as each becomes a separate object. 


#### More Live Path Effects changes

You'll see a number of fixes within the LPEs in Inkscape and Boolean Operations is no longer experimental. Look for the Fill between many LPE in the Path menu to more efficiently fill variable-width paths created using PowerStroke, and find improvements to the Offset and Taper Stroke LPEs. This work on LPEs was done by  Jabier Arraiza.


#### Choose a file extension for exporting 

The Export PNG Image Dialog now exports PNGs with one click instead of two. You can also export in different file formats, namely JPG, WebP and TIFF, along with Optimized PNG.



#### All About Inkscape

Our newly revamped 'About Inkscape' Dialog features a larger window (to show off our fabulous contest winner's art) and a tabbed interface. Inkscape authors and translators are listed in tabs along with one called License that briefly explains Inkscape licensing. To help you help the project with your feedback, you can now copy version and debugging info with a single click.



#### Choose your language

Inkscape is available in 88 languages, though not all of them are complete. Updates to some 22 languages were made for Inkscape 1.1 If this is something you'd like to help the project complete, get in touch!  https://inkscape.org/contribute/translations/



#### macOS changes

You'll notice a new Inkscape icon and be able to import Visio files. We're always working on improving the program for you. If you're a developer in the macOS space and you are interested in contributing to the project, get in touch! https://chat.inkscape.org/channel/team_devel_mac



#### Managing Extensions: Under Construction

We're beta-testing a new Extension Manager within Inkscape, which allows you to access a repository with community-vetted (Python) extensions on the Inkscape.org website from inside Inkscape. Note for Windows users, the Extensions Manager comes as an optional installable component. If you're an extension developer and would like to request a review, contact Inkscape's Extensions Team in our chat. https://chat.inkscape.org/channel/inkscape_extensions This manager was developed by Martin Owens.



#### Bugs to be tested & migrated

As usual, plenty of effort has gone into wrangling a whole host of bugs that caused crashes of all kinds and other troubles, affecting functionality such as rotating, copy-and-paste, or causing freezes and memory leaks.  If you have some free time and enjoy the challenge of solving issues, we would love to hear from you! Join us in the development chat [https://chat.inkscape.org/channel/team_devel] and we'll tackle them together.

Alternatively, if you prefer to simply flag an issue for us, please send us a bug report [https://inkscape.org/report].


**For more details on specific updates in Inkscape 1.1, check out the [Release Notes](https://wiki.inkscape.org/wiki/index.php/Release_notes/1.1) .**

### Download Inkscape 1.1 now

Head here to download Inkscape 1.1 for Linux, Windows or macOS https://inkscape.org/release/inkscape-1.1/?latest=1

Don't forget to watch the VIDEO - LINK for a walkthrough of some of the highlights and updates.



### Reach out & join the community

Questions, comments and feedback on Inkscape are welcome. For user support, head to [Inkscape's chat](https://chat.inkscape.org/channel/inkscape_user), where community volunteers field help requests of all sorts. To report bugs, fill out a report directly in the [Inbox](https://gitlab.com/inkscape/inbox/-/issues/).

From programmers and translators to writers, researchers and artists, the Inkscape community is a friendly space to learn and share, build skills and meet others interested in free software and vector editing. Visit us at Inkscape's [Community Page](https://inkscape.org/community/) for ways to connect.

#### ***Draw Freely!***
