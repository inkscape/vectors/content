## Seeking macOS dev contributor for Inkscape project



While our software runs on Linux, Windows and macOS, we know it doesn't run evenly across all three platforms. The macOS version is in need of some serious love.

That is why we are seeking to recruit a volunteer contributor to help support the project internally. Here is our pitch. 

Are you a macOS developer? Can you see yourself contributing to an open source project? We know this is a huge ask, but rest assured, you'll be joining like-minded people who all put their heart and time into the project.

Specifically, we need someone to analyze the macOS-specific performance bottlenecks in Inkscape: perform a performance analysis to identify the culprits in our code or dependencies and, ideally, come up with ideas and/or patches to improve the current situation. 

Although this won't be rocket science, you should expect this task to take time and motivation on your end to get results. 

Besides C/C++ experience and owning a Mac, nothing else is required but your motivation and precious time. 

Unless you already are an expert in GTK and macOS low level APIs, you'll have to acquire the required knowledge in the "learning by doing" way. We do not have this kind of knowledge in the project and are seeking someone to help develop it with us. 

This is part of the fun of being a contributor with Inkscape - the opportunity to learn and experiment. That's why time and motivation on your part are key.

Still, let it be said that most of us are volunteers at Inkscape. There's no predetermined solution on how we expect the performance problems to be solved. 

There are several paths that could be undertaken to get the job done:

- identify performance-wasting code in Inkscape/GTK/Cairo/... and create patches to optimize 
- migrate codebase from GTK 3 to GTK 4, betting on the new improved macOS backend in GTK 4
- introduce a GPU renderer
- whatever you can come up with!

Basically, whatever helps to improve the situation will have a huge impact on Inkscape and our users. 

If you are interested, have questions or other ideas, come say hello in #team_devel_mac [https://chat.inkscape.org/channel/team_devel_mac]. (Don't be shy, no strings attached!)
