Inkscape 1.0 release - April 2020 interview

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*sam, 25/04/2020 - 22:33*

*0.0.0.0*



**Contributor Interview: doctormo**



Headline 

**"I'm motivated to help others who need better Free Software"**



**Please introduce yourself; what is your name and where in the world do you live?**

*Martin Owens - can we say your name and where you live??* 



**When did you first meet the Inkscape Community? Please explain how you came to join the project?**

I can find evidence of me posting to the Inkscape mailing list in 2006. I was interested in contributing my perl based barcode extension to Inkscape.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

I'd say development, the board of directors, the website and the user experience (ux).



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

The community is friendly and passionate. Two things I feel myself are important. I'm motivated to help others who need better Free Software. Solidarity with the wider Free Software ecosystem and movement is important to me.



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those ones?**

The biggest contribution has been the python extensions refactoring. This was a much needed overhaul of the python extensions framework. We needed to improve three things: the maintainability, the testability and the ease of use. This isn't something users will see much in the 1.0, but going forwards it should make extensions we ship much more robust and less error prone.



**What's your favorite part of the 1.0 release?**

I like the themes and the HiDPI support.



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

All the stuff on my todo list.



**What would be your best advice to anyone wanting to contribute to the Inkscape project?**

Make friends. Inkscape's community is like all volunteer community, it's driven by relationships. If you help people out with a problem they might have, you can form a strong motivation to continue working on Inkscape in yourself.



_________________________

Is there anything else you'd like to tell us?

Thanks Maren.

**We'd like to tag you on social media if we use quotes from your interview. If you are on any of the following platforms, please add your handle below:**

Mastodon:
Twitter: realdoctormon
Facebook:
Instagram:
deviantArt: doctormo

How can we contact you for details / clarifications?Rocket