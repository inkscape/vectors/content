# Stable Inkscape 0.92.5 released and Testers needed for Inkscape 1.0 Release Candidate

<h3>Inkscape 0.92.5</h3>

While we are working to release Inkscape 1.0 in the very near future, we have also addressed some issues with the 0.92 series. Most notably, an annoying issue for Windows 10 users that prevented Inkscape from discovering user-specific fonts has now been fixed. Python extensions will now also run with Python 3. <a href="https://inkscape.org/release/inkscape-0.92.5/">Read all about the changes in the latest stable Inkscape version and download it from our website</a>.

<h3>Call for Testing the 1.0 Release Candidate</h3>

Inkscape 1.0 will mark a major milestone for the project. This release is loaded with features and polish! You can find <a href="https://wiki.inkscape.org/wiki/index.php?title=Release_notes/1.0">a draft of the release notes that lists all the goodies in our Wiki</a>. In preparation for a stable release for Windows and Linux please <a href="LINK_NEEDED">download and test our first Release Candidate</a>.

Inkscape 1.0 for macOS will feature the easiest installation and best OS integration Inkscape has ever offered. There are, however, unique issues that will require additional testing and improvement for our Apple fans. Please get involved by <a href="LINK_FOR_MACOS">downloading and testing the latest beta release</a>.

<h3>Report Bugs</h3>

If you run into any bugs, especially with the Release Candidate, please help us by <a href="https://inkscape.org/report">reporting them promptly</a>, so we can try to fix them before we release the final 1.0 version. 

<h3>Join Us</h3>

We hope we can do our part to make your life a bit easier in these special days and that you enjoy using Inkscape just as much as we enjoyed creating it. In times of self-isolation and stay-at-home orders you're more welcome than ever to to join our online community so we can safely bridge the gaps of social distancing together.