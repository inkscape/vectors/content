### **Inkscape contributor organizing Gimpscape's inaugural Artweek**



Open source software artists and designers have a new place to learn online, thanks to the Gimpscape design community's inaugural Artweek, an online event taking place on June 27-28, 2020. 

[Gimpscape](https://gimpscape.org) is an Indonesian-based design community that focuses on users of open source software (FOSS). Founded in February 2016 from Indonesian user communities for open source software GIMP and Inkscape, it has evolved into a broader OSS design space whose motto is "Learn, Share and Inspire". 

Inkscape contributor Rania Amina, a cofounder of Gimpscape, is heading up the volunteer team behind Artweek. Amina is a graphic designer and user interface (UI) designer from Indonesia's Yogyakarta Special Region.

The design community (@gimpscape on Telegram) offers members monthly challenges, promotes creative members' products and services, shares workshop and tutorials, along with managing sponsorship and funding opportunities.

Rania Amina is excited to bring together two days of brief presentations and workshops highlighting different open source software and to give back to the communities through participant registration fees. "Thanks to all the developers and people behind the projects," he said. 

Artweek event will feature 20 workshops and short presentations over two days, including several about Inkscape. They range from a lightning talk on translation, a presentation on Building Inkscape Community in Campus: Challenge and Opportunity, and a tutorial on character design to using Inkscape to create visuals for Instagram campaigns. More details can be found [online](https://artweek.gimpscape.org/en/schedule.html).

All presentations and talks will feature English-language visuals (PDF) created in OSS, while speakers will present in different Indonesian languages.

Registration is a $10 for anyone outside of Indonesia (IDR 20,000 within Indonesia), which will be directed to open source projects, such as Inkscape, GIMP, Krita and others. For more information on that, head to Gimpscape's  [**Donation Report page**](https://artweek.gimpscape.org/en/donation-report.html). 

For more information and to register by the June 26 deadline, visit the English-language [Artweek page]( https://artweek.gimpscape.org/en/about.html#complete ). 



Add Gimpscape logo

