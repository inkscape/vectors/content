<!-- This is for *short* news items to be added to the main website -->

## Title
Proposed title for the news item here.

## Body
Draft the text for the news item here.

## Tasks
* [ ] Preliminary draft
* [ ] Review and copy edit
* [ ] Final draft
* [ ] Add to website, unpublished
* [ ] Publish the news item to the main website

## Additional Publication Channels
<!-- remove spaces between @ and name when it's time to distribute the tasks -->

* [ ] Twitter post ( @ prkos )
* [ ] Facebook group post ( @ CRogers )
* [ ] Mastodon post ( @ Moini )
* [ ] Forums ( @ Lazur or @ pacecraft )
* [ ] deviantArt ( @ Lazur )
* [ ] facebook (@ Lazur )
