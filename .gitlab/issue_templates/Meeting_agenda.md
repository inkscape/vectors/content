<!-- This is for Team vector monthly meetings agenda -->

## Time & Place
* Month 0, 202x, 6:00-7:00 PM UTC (See: https://inkscape.org/*inkscape-vectors/calendar/)
* Rocket.Chat (https://chat.inkscape.org/channel/team_vectors)

## Vectors Meeting Agenda (1 Hour)
<!-- remove spaces between @ and name for subject holders -->
* Meta:
    - Introductions
    - Access grants
* Status reports:
    - Social Media
    - Sub-topic 2...
* Current items:
    - Sub-topic 1
        - Sub-sub topic 1
        - Sub-sub topic 2...
    - Sub-topic 2...
* Topic:
    - Sub-topic 1
        - Sub-sub topic 1
        - Sub-sub topic 2...
    - Sub-topic 2...

## Tasks
<!-- remove spaces between @ and name when it's time to distribute the tasks -->
* [ ] Task 1
* [ ] Task 2
* [ ] Task 3
