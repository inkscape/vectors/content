## Synopsis
A short description of the article here.

## Audience
Who will be interested in this article here.

## Title
Proposed title for the article here.

## Author
Suggested author of the article here.

## File
[Add link to content repo file https://gitlab.com/inkscape/vectors/content/-/tree/master/articles/...]

## Publication Channels
<!-- remove spaces between @ and name when it's time to distribute the tasks -->

* [ ] Website
* [ ] Twitter post ( @ prkos )
* [ ] Facebook group post ( @ CRogers )
* [ ] Mastodon post ( @ Moini )
* [ ] Forums ( @ Lazur or @ pacecraft )
* [ ] deviantArt ( @ Lazur )
* [ ] facebook (@ Lazur )
